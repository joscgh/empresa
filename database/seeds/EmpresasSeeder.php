<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmpresasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        for ($i=0; $i < 50; $i++) {
            \DB::table('empresa')->insert(array(
                   'nombre' => 'empresa ' . $faker->firstNameFemale . ' ' . $faker->lastName,
                   'tipologia'  => $faker->randomElement(['Sociedad anonima','Compañia anonima','SRL']),
                   'pais' => 'Venezuela',
                   'estado' => 'Zulia',
                   'ciudad'=>'Maracaibo',
                   'email_user' => 'joscgh@gmail.com',
                   'passw' =>'123456',
                   'descripcion' => 'djfdlskfjsdlkjfdsklj'
            ));
        }
    }
}
