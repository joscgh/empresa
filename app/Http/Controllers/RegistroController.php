<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registro;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $path = "registro";

    public function index()
    {
        //
        $data = Registro::all();
        //Enviamos esos registros a la vista.
        return view($this->path.'.index', compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view($this->path.'.create');
    }

    public function listedit()
    {
        //
        $data = Registro::all();
        return view($this->path.'.listedit', compact('data'));
    }

    public function eliminar()
    {
        //
        $data = Registro::all();
        return view($this->path.'.eliminar', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $empresa = new Registro();
        
            $empresa->nombre = $request->nombre;
            $empresa->tipologia = $request->tipologia;
            $empresa->pais = $request->pais;
            $empresa->estado = $request->estado;
            $empresa->ciudad = $request->ciudad;
            $empresa->email_user = $request->email;
            $empresa->passw = $request->pass;
            $empresa->descripcion = $request->descripcion;
            $empresa->created_at = date('Y-m-d H:i:s');
            $empresa->updated_at = date('Y-m-d H:i:s');
            $empresa->save();

            return redirect()->route('registro.index');
        } catch (Exception $e) {
            return "Fatal error... ";$e.getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $empresa = Registro::findOrFail($id);
        return view($this->path.'.edit', compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $empresa = Registro::findOrFail($id);

        $empresa->nombre = $request->nombre;
        $empresa->tipologia = $request->tipologia;
        $empresa->pais = $request->pais;
        $empresa->estado = $request->estado;
        $empresa->ciudad = $request->ciudad;
        $empresa->email_user = $request->email;
        $empresa->passw = $request->pass;
        $empresa->descripcion = $request->descripcion;
        $empresa->created_at = date('Y-m-d H:i:s');
        $empresa->updated_at = date('Y-m-d H:i:s');
        $empresa->save();

        return redirect()->route('registro.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $empresa = Registro::findOrFail($id);
            $empresa -> delete();
            return redirect()->route('registro.index');

        } catch (Exception $e) {
            return "Fatal error... ";$e.getMessage();
        }
    }
}
