@extends('empresa')
@section('estilos')<link rel="stylesheet" href="../CSS/estilos.css">@stop
@section('contenido')
<div class="col-md-12">
<h3>LISTA DE EMPRESAS</h3>
<hr>
<div class="table-responsive">
<table class="table">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Tipologia</th>
			<th>Pais</th>
			<th>Estado</th>
			<th>Ciudad</th>
			<th>Email/Usuario</th>
			<th>Descripcion</th>
		</tr>
	</thead>
	<tbody>
	@foreach ($data as $row)
		<tr>
			<td>{{ $row->nombre }}</td>
			<td>{{ $row->tipologia }}</td>
			<td>{{ $row->pais }}</td>
			<td>{{ $row->estado }}</td>
			<td>{{ $row->ciudad }}</td>
			<td>{{ $row->email_user }}</td>
			<td>{{ $row->descripcion }}</td>
			<td>
			<form action="{{route('registro.destroy', $row->id)}}" method="post">
				<input type="hidden" name="_method" value="DELETE">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<button class="btn btn-danger">Eliminar</button>
			</form>
		</tr>
	@endforeach
	</tbody>
</table>
</div>
</div>
@stop