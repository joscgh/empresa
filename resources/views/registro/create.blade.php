@extends('empresa')
@section('estilos')<link rel="stylesheet" href="../CSS/estilos.css">@stop
@section('contenido')
<form method="post" action="{{ route('registro.store') }}">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="col-md-10"><h3>REGISTRO DE EMPRESAS</h3></div>
  <div class="col-md-2" style="margin-top:12px;">
    <input type="submit" name="" value="Guardar Cambios" class="btn btn-primary"></div>
  <div class="col-md-12"><hr></div>
  <div class="col-md-8">
    <h4>INFORMACIÓN BÁSICA</h4>
    <div class="back-color-b" style="padding:8px 10px 15px 10px">
    <div class="row">
	     <div class="col-md-6">
        <h4>Nombre comercial de la empresa</h4>
        <input type="text" name="nombre" required>
       </div>
	   <div class="col-md-6"> 
        <h4>Tipología de la empresa</h4>
            <select id="tipologia" name="tipologia">
              <option value="Sociedad anonima (SA)">Sociedad anonima (SA)</option>
              <option value="Compañia anonima (CA)">Compañia anonima (CA)</option>
            </select>
      </div>
    </div>
    <div class="row">
            <div class="col-md-4">
            <h4>Pais</h4>
            <select id="" name="pais">
              <option value="Argentina">Argentina</option>
              <option value="Bolivia">Bolivia</option>
              <option value="Chile">Chile</option>
              <option value="Colombia">Colombia</option>
              <option value="Ecuador">Ecuador</option>
              <option value="Mexico">Mexico</option>
              <option value="Peru">Perú</option>
              <option value="Venezuela">Venezuela</option>
            </select>
            </div>
            <div class="col-md-4">
            <h4>Estado</h4>
            <input type="text" name="estado" required>
            </div>
            <div class="col-md-4">
            <h4>Ciudad</h4>
            <input type="text" name="ciudad" required>
            </div>
    </div>
    </div>
    <br>
    <h4>INFORMACIÓN REGISTRO</h4>
    <div class="back-color-b" style="padding:8px 10px 15px 10px">
    <div class="row">
      <div class="col-md-4">
        <h4>Email/Usuario</h4>
        <input type="email" name="email" required>
      </div>
      <div class="col-md-4">
        <h4>Password</h4>
        <input type="password" name="pass" required>
      </div>
      <div class="col-md-4">
        <h4>Repetir Password</h4> 
        <input type="password" name="pass" required>
      </div>
    </div>
    </div>
    </div>
            <div class="col-md-4" style="margin-top:40px">
            <div class="col-md-12 back-color-b" style="padding:8px 10px 8px 10px;">
              <h4>Descripcion de la empresa</h4>
              <textarea name="descripcion" style="width:100%;height:239px;resize:none" placeholder="255 caracteres"></textarea>
            </div>
            </div>

</form>
@stop