<!DOCTYPE html>
<html>
<head>
	<title>Prueba PHP</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>-->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   @yield('estilos')
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="" style="margin-top:35px">
		
      <!-- Static navbar -->
      <nav class="navbar navbar-default" role="navigation">
        <div class="">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#">INICIO</a></li>
              <li class="desk"><a>|</a></li>
              <li class="active dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EMPRESAS <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('registro.index') }}">LISTA DE EMPRESAS</a></li>
                  <li><a href="{{ route('registro.create') }}">REGISTRO DE EMPRESAS</a></li>
                  <li><a href="{{ url('registro/listedit') }}">ACTUALIZACION DE EMPRESAS</a></li>
                  <li><a href="{{ url('registro/eliminar') }}">ELIMINACIÓN DE EMPRESAS</a></li>
                </ul>
              </li>
              <li class="desk"><a>|</a></li>
              <li><a href="#">SERVICIOS</a></li>
              <li class="desk"><a>|</a></li>
              <li><a href="#">VALORA UN SERVICIO</a></li>
              <li class="desk"><a>|</a></li>
              <li><a href="#">CONTACTO</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right" style="background-color:#5bc0de">
              <li><a href="#"  style="color:#fff">Registro</a></li>
              <li class="desk"><a style="color:#fff">|</a></li>
              <li><a href="#" style="color:#fff">Iniciar sesión</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      </div>
      <div class="col-md-12 back-color-w" style="margin-top:20px;padding-bottom:40px;padding-top:8px">
         @yield('contenido')
      </div>
			<div class="col-md-12 back-color-w" style="padding-top:10px;margin-top:40px;margin-bottom:60px;color:#aaa">
        <div class="row">
        <div class="col-md-12">
          <div class="col-md-2"></div>
          <div class="col-md-10" style="padding:0">
				    <div class="col-md-3"><h4>Empresas</h4><p>Consequat<br>Nunc tempor<br>Ante quis libero<br>Fermentum</p></div>
				    <div class="col-md-3"><h4>Servicios</h4><p>Set congue suscipit<br>Consequat<br>Nunc tempor<br>Ante quis libero<br>Fermentum</p></div>
				    <div class="col-md-3"><h4>Valora un servicio</h4><p>Set congue suscipit<br>Consequat<br>Nunc tempor<br>Ante quis libero<br>Fermentum<br>Rhoncus dolor mattis<br>Nullam pellentesque<br>interdum ipsum ac</p></div>
				    <div class="col-md-3"><h4>Contacto</h4><p>Ante quis libero<br>Fermentum<br>Rhoncus dolor mattis<br>Nullam pellentesque<br>interdum ipsum ac</p></div>
          </div>
        </div>
        </div>
        <div class="row">
          <br><br>
          <div class="col-md-12">
          <div class="col-md-2"></div>
				  <div class="col-md-10"><p>Opiniones y consejos sobre servicios sanitarios, funerarios y mucho mas...<br>&copy; <?=date('Y')?> Todos los derechos reservados. <a href="#">Condiciones de uso</a>, <a href="#">Polítca de privacidad</a> y <a href="#">Política de cookies</a><br><br>Set tincidunt velit a accumsan laoreet. Donec vitae est quis ante rhoncus cursus non id ante. Nam fringilla euismod dui, non ullamcorper nunc.<br>Nullam id vestibulum orci, id porttitor lectus. Maecenas scelerisque dignissim erat et faucibus. Cras malesuada non massa ac lobortis.</p>
          </div>
        </div>
        </div>
        <div class="row">
          <br><br>
				  <div class="col-md-12" style="text-align:center">VERITUS&copy; 2017</div>
          <br><br>
        </div>
        </div>
			</div>
		</div>
	</div>
</body>
</html>